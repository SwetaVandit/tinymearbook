﻿/**
 * Project : Tinyme AR Book
 * Authours: Karlotta Varieur, Thanura Siribaddana
 * Date : 24 Nov 2016
 * */
using UnityEngine;
using System.Collections;

public class GroupManager : MonoBehaviour {
	public GameObject[] objects;
	public AudioClip ReadToMeAudio;
	public AudioClip BackgroundMusic;

	const string IdleAnimName = "idle";
	const float AnimDelayTime = 2.0f;

	int objectCount = 0;
	int currentIndex = 0;
	PageManager mPage;
	//ObjectManager objManager;


	// Use this for initialization
	void Awake () {
		objectCount = objects.Length;

		hideObjects ();
		Debug.Log ("Group onStart");
	}

	// Update is called once per frame
	void Update () {

	}

	IEnumerator showObject () {
		if (currentIndex >= objectCount)
			StopCoroutine ("showObject");

		Debug.Log (string.Format("Object{0} opened.",currentIndex));
	
		if (objects [currentIndex].GetComponent<ObjectManager> () != null) {

			Debug.Log (string.Format("Object{0} StartCoroutine.",currentIndex));
			
			StartCoroutine(objects [currentIndex].GetComponent<ObjectManager> ().ShowObject (objects [currentIndex]));

		} else {

			Debug.Log (string.Format("Object{0} SetActive.",currentIndex));
			
			objects [currentIndex].SetActive (true);

		}

		yield return new WaitForSeconds (0);

		currentIndex++;
		if (currentIndex < objectCount) {
			StartCoroutine (showObject ());
		} else {
			currentIndex = 0;
			mPage.OnGroupAppeared ();
		}
	}

	public void OnLoad () {

		if (AudioManager.Instance != null) {

			if (ReadToMeAudio != null) {

				AudioManager.Instance.setAudio (ReadToMeAudio);

			} else {

				AudioManager.Instance.setAudio (null);
			}


			if (BackgroundMusic != null) {

				AudioManager.Instance.setBgAudio (BackgroundMusic);

			} else {
				
				AudioManager.Instance.setBgAudio (null);
			}

		}
		
	}
		
	public void setPage (PageManager page) {
		mPage = page;
	}

	public void showObjects () {
		
		Debug.Log ("Group opened.");
		currentIndex = 0;
		OnLoad ();
		StartCoroutine (showObject ());
	}

	public void hideObjects () {
		Debug.Log ("Group closed.");
		StopAllCoroutines ();
		for (int i = 0; i < objectCount; i++) {
			objects [i].SetActive (false);
		}

		currentIndex = 0;
	}
}
