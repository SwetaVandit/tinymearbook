﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;
using System;
using Vuforia;
//using cn.sharerec;


public class ScreenCaptureManager : MonoBehaviour {
	public AudioClip sndBtnTouch;
	

    public GameObject audioManager;
    public GameObject UI;
    public GameObject captureImage;
    public GameObject mediaPanel;

    //이벤트 및 델리게이트 선언
    public delegate void OnCaptureCompletedHandler();
    public static event OnCaptureCompletedHandler OnCaptureCompleted;
    
    //	public Capture capture;

#if UNITY_IPHONE
	[DllImport("__Internal")]
	private static extern bool saveToGallery (string path);
	[DllImport("__Internal")]
	private static extern bool saveToVideo(string path);
	[DllImport("__Internal")]
	private static extern void _TAG_ShareTextWithImage (string iosPath, string message);

/*	[DllImport ("__Internal")]
	public static extern void __setTag (string tag);
	[DllImport ("__Internal")]
	public static extern void __delTag (string tag);*/

#endif

    public static void SetTag(string tag)
	{	
		
	}
	public static void delTag(string tag){
		
	}

	// Use this for initialization
	void Start () {
		//msgBox = new Message ();
		#if UNITY_ANDROID
		string dirPath = "mnt/sdcard/DCIM/" + "Tiny";	//"mnt/sdcard/DCIM/"

		if (!Directory.Exists(dirPath))
		{
			Directory.CreateDirectory(dirPath);
		}
		#endif
	}
		
	void OnDestroy(){

	}

	// Update is called once per frame
	void Update () {
		
	}

	public void OnBtnCapture(){
        if (ToolBarManager.isRecording)
            return;
        mediaPanel.SetActive(false);
        StartCoroutine(DoCapture());        
        //DoCapture();
	}

    IEnumerator DoCapture()
    {
        UI.SetActive(false);

#if UNITY_IPHONE
		Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.WhiteLarge);
#elif UNITY_ANDROID
        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif
        Handheld.StartActivityIndicator();

        yield return new WaitForSeconds(0.5f);
        audioManager.GetComponent<AudioSource>().PlayOneShot(sndBtnTouch);
        StartCoroutine(screenCapture());     
    }
    
    public IEnumerator screenCapture()
	{
		yield return new WaitForEndOfFrame ();
		Texture2D captureTexture;
		captureTexture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, true); // texture formoat setting
		captureTexture.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0, true); // readPixel
		captureTexture.Apply (); // readPixel data apply
		Global.texTempImage = captureTexture;
	   

		byte[] dataToSave = captureTexture.EncodeToPNG();
		Global.destination = getCaptureName ();//
		#if !UNITY_WEBPLAYER
		File.WriteAllBytes(Global.destination, dataToSave);                //capture 자료 파일로 보관
        #endif
               
        yield return new WaitForSeconds(2f);
#if UNITY_IPHONE
        Handheld.StopActivityIndicator();
#elif UNITY_ANDROID
        Handheld.StopActivityIndicator();
#endif
        OnCaptureSave();
        OnCaptureCompleted();        
    }

    public static void OnCaptureSave()
    {
#if UNITY_ANDROID
        string CapturePath = Path.Combine("mnt/sdcard/DCIM/Tiny", System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".jpg");
        File.Copy(Global.destination, CapturePath);
        //goCaptureUI.SetActive (false);
        if (File.Exists(Global.destination))
            File.Delete(Global.destination);
        RefreshPicture(CapturePath);
#elif UNITY_IPHONE
		RefreshPicture(Global.destination);
//		goCaptureUI.SetActive (false);
#endif
    }
		
    public string getCaptureName()
	{
		string dirPath = "";
		#if UNITY_IPHONE
		dirPath = Application.persistentDataPath + "/" + "Tiny";
		if (!Directory.Exists(dirPath))
		{
			Directory.CreateDirectory(dirPath);
		}
		return string.Format("{0}/capture_{1}.png", dirPath, System.DateTime.Now.ToString("yyyyMMdd_HHmmss"));
		#elif UNITY_ANDROID	
		dirPath = "mnt/sdcard/DCIM/" + "Tiny";	//"mnt/sdcard/DCIM/"
		if (!Directory.Exists(dirPath))
		{
			Directory.CreateDirectory(dirPath);
		}
		return string.Format("{0}/capture_{1}.png", dirPath, System.DateTime.Now.ToString("yyyyMMdd_HHmmss"));
		//	return string.Format("mnt/sdcard/DCIM/Camera/numberaa.png");
		#else
		if( Application.isEditor == true ){ 
			dirPath = "mnt/sdcard/DCIM/";
			if (!Directory.Exists(dirPath))
			{
				Directory.CreateDirectory(dirPath);
			}
			return string.Format("{0}/capture_{1}.png", dirPath, System.DateTime.Now.ToString("yyyyMMdd_HHmmss"));
		} 
		#endif	
		return "";
	}

	public static void RefreshPicture(string RefreshPath)
	{
		bool photoSaved = false;
		#if UNITY_ANDROID
		//REFRESHING THE ANDROID PHONE PHOTO GALLERY IS BEGUN
		try {
			AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");        
			AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");        
			AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2]{"android.intent.action.MEDIA_MOUNTED", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + RefreshPath)});        
			objActivity.Call ("sendBroadcast", objIntent);
		}catch (System.Exception) {
			try{
				AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");        
				AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");        
				AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2]{"android.intent.action.MEDIA_SCANNER_SCAN_FILE", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + RefreshPath)});        
				objActivity.Call ("sendBroadcast", objIntent);
			}catch(System.Exception){
				AndroidJavaClass obj = new AndroidJavaClass("com.ryanwebb.androidscreenshot.MainActivity");
				while(!photoSaved) 
				{
					photoSaved = obj.CallStatic<bool>("addImageToGallery", RefreshPath);
					/*	yield return *///StartCoroutine(Wait(.5f));
				}
			}
		}
		//REFRESHING THE ANDROID PHONE PHOTO GALLERY IS COMPLETE
		
		#elif UNITY_IPHONE
		while(!photoSaved) 
		{
			photoSaved = saveToGallery(RefreshPath);			
		}
		UnityEngine.iOS.Device.SetNoBackupFlag(RefreshPath);
		#endif
	}

	public static IEnumerator Wait(float delay)
	{
		float pauseTarget = Time.realtimeSinceStartup + delay;
		while(Time.realtimeSinceStartup < pauseTarget){
			yield return null;	
		}
	}
}
