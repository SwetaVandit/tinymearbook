﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Global {
	public readonly static string AppVer = "1.0.6";

	public static string UPDATE_URL = "http://218.36.125.250:8186/AppVersion.xml";
	public static string SERVER_URL =  "http://218.36.125.250:8186/Account/getXML.aspx?content_id=";
	public static string CurMarkerName;

	public static Vector3	ARCampos;

	public static GameObject loopObject;
	public static bool bCanGetTexture;
	public static bool bExistModelOnScreen;
	public static string trackedName;
    public static Texture2D texTempImage;
    public static string destination;
}
