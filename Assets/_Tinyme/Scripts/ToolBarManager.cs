﻿/**
 * Project : Tinyme AR Book
 * Authours: Karlotta Varieur, Thanura Siribaddana
 * Date : 24 Nov 2016
 * */
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;

public class ToolBarManager : MonoBehaviour {
	public static ToolBarManager Instance;

    public GameObject toolBar;
    public GameObject sharePanel;
    public GameObject mediaPanel;
    public GameObject captureImage;
    public GameObject timerPanel;
    public Text textrecordtime;

    public Button videoBtn;
    public Slider sldVolume;
	public Button btnReadToMe;
	public Button btnNext;
	public Button btnPrev;
    public Button btnCapture;

	public Sprite imgPlay;
	public Sprite imgPause;
    public Sprite imgStartRecord;
    public Sprite imgStopRecord;

	PageManager currentPage;

	float volume = 0.5f;
	bool isSoundOn = true;
	bool isAudioFinished = false;
	int groupCount = 0;
    public static bool isRecording;
    int recordTime;
    bool isReadyForRecording = false;

	#if UNITY_IPHONE
	[DllImport("__Internal")]
	private static extern bool saveToGallery (string path);
	[DllImport("__Internal")]
	private static extern bool saveToVideo(string path);
	[DllImport("__Internal")]
	private static extern void _TAG_ShareTextWithImage (string iosPath, string message);

	/*	[DllImport ("__Internal")]
	public static extern void __setTag (string tag);
	[DllImport ("__Internal")]
	public static extern void __delTag (string tag);*/

	#endif

	// Use this for initialization
	void Awake () {
		Instance = this;

		//sldVolume.value = volume;
		btnReadToMe.interactable = false;
		btnNext.interactable = false;
		btnPrev.interactable = false;
        btnCapture.interactable = false;

        isSoundOn = false;
		btnReadToMe.image.sprite = imgPlay;
        isRecording = false;

    }

    void Start()
    {
        ScreenCaptureManager.OnCaptureCompleted += OnCaptureCompleted;

        Everyplay.ReadyForRecording += OnReadyForRecording;
    }

	// Update is called once per frame
	void Update () {

	}

    public void OnReadyForRecording(bool enabled)
    {
        isReadyForRecording = enabled;
        if (enabled)
        {
            // The recording is supported
            Debug.Log("Ready for Recording");
        }
    }


    public void OnPageOpened (PageManager page) {
		currentPage = page;
		btnReadToMe.interactable = true;
        btnCapture.interactable = true;
		groupCount = currentPage.groups.Length;

		if (groupCount > 1) {
			
			btnNext.interactable = true;

		}

		if (AudioManager.Instance != null) {
			
			AudioManager.Instance.playAudio ();
			AudioManager.Instance.playBgAudio ();

		}
			
		isSoundOn = true;
		isAudioFinished = false;
		btnReadToMe.image.sprite = imgPause;
	}

	public void OnPageClosed () {
		btnReadToMe.interactable = false;
		btnNext.interactable = false;
		btnPrev.interactable = false;
        btnCapture.interactable = false;

		AudioManager.Instance.stopAudio ();
		AudioManager.Instance.stopBgAudio ();

		isSoundOn = false;
		btnReadToMe.image.sprite = imgPlay;
	}

	public void OnInfo () {
	}

	public void OnReadToMe () {
		
		if (isAudioFinished) {
			
			isAudioFinished = false;

		} else {
			
			isSoundOn = !isSoundOn;

		}

		if (isSoundOn) {
			
			AudioManager.Instance.playAudio ();

			btnReadToMe.image.sprite = imgPause;

		} else {
			
			AudioManager.Instance.pauseAudio ();

			btnReadToMe.image.sprite = imgPlay;

		}
	}

	public void OnVolumeChange (float value) {
		
		volume = value;
		AudioManager.Instance.setVolume (volume);
		AudioManager.Instance.setBgVolume (volume);

	}
		
	public void OnPrev () {
		
		bool prevPossible = currentPage.showPrevGroup ();

		if (isSoundOn) {
			
			AudioManager.Instance.playAudio ();
			btnReadToMe.image.sprite = imgPause;

		}

		//Play background Music
		AudioManager.Instance.playBgAudio ();

		isAudioFinished = false;

		btnNext.interactable = true;

		if (!prevPossible) {
			
			btnPrev.interactable = false;

		}
	}

	public void OnNext () {
		
		bool nextPossible = currentPage.showNextGroup ();

		if (isSoundOn) {
			
			AudioManager.Instance.playAudio ();
			btnReadToMe.image.sprite = imgPause;

		}

		//Play background Music
		AudioManager.Instance.playBgAudio ();

		isAudioFinished = false;

		btnPrev.interactable = true;

		if (!nextPossible) {
			
			btnNext.interactable = false;

		}

	}

	public void OnAudioEnd () {
		
		isAudioFinished = true;
		btnReadToMe.image.sprite = imgPlay;

	}


	public void OnUrlClick () {
		
		Application.OpenURL("https://www.tinyme.com");

	}

    public void OnCaptureClick()
    {
        if (isRecording)
            return;
        //capture screenshot
        mediaPanel.SetActive(true);
        toolBar.SetActive(false);
    }

    public void OnShareClick()
    {
#if UNITY_ANDROID
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");    //  공유하기 위한 각종 설정
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + Global.destination);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
        intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");

        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        currentActivity.Call("startActivity", intentObject);
#elif UNITY_IPHONE
		_TAG_ShareTextWithImage(Global.destination,"");
#endif
    }

    public void OnReturnClick()
    {
        captureImage.SetActive(false);
        sharePanel.SetActive(false);
        toolBar.SetActive(true);

        File.Delete(Global.destination);
    }

    public void OnCaptureCompleted()
    {
        //Print Screenshot on CaptureImage GameObject
        captureImage.SetActive(true);
        Debug.Log("Capture Completed");
        Sprite sprite = Sprite.Create(Global.texTempImage, new Rect(0, 0, Global.texTempImage.width, Global.texTempImage.height), new Vector2(0.5f, 0.5f), 40);
        UnityEngine.UI.Image imageComponent = captureImage.GetComponentInChildren<UnityEngine.UI.Image>();
        imageComponent.sprite = sprite;

        sharePanel.SetActive(true);
    }

    //Record
    public void OnStartRecord()
    {
        bool bsupported = Everyplay.IsSupported();
        Debug.Log("everyplay supported = " + bsupported + " ready for recording: " + isReadyForRecording);
        
        if (bsupported)
        {
            if (!isRecording)
            {
                //start recording
                Everyplay.StartRecording();
                Everyplay.FaceCamSetRecordingMode(Everyplay.FaceCamRecordingMode.PassThrough);
                
                isRecording = true;
                videoBtn.image.sprite = imgStopRecord;
                timerPanel.SetActive(true);
                recordTime = 0;
                StartCoroutine(RecordingProcess());
            }
            else
            {
                //stop recording
                Everyplay.StopRecording();
                Global.destination = FindMP4File();   // mp4 파일 찾는 함수
                Debug.Log("Destination: " + Global.destination);
				isRecording = false;
				videoBtn.image.sprite = imgStartRecord;

				mediaPanel.SetActive(false);
				timerPanel.SetActive(false);

				textrecordtime.text = "00:00:00";

				StartCoroutine(SavingProcess());
            }
        }
    }

    IEnumerator SavingProcess()
    {
		yield return new WaitForSeconds (.5f);
#if UNITY_IPHONE
		saveToVideo(Global.destination);
#else
		string CapturePath = Path.Combine("mnt/sdcard/DCIM/Tiny", System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".mp4");
		File.Copy(Global.destination, CapturePath);
#endif

#if UNITY_IPHONE
		Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.WhiteLarge);
#elif UNITY_ANDROID
        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif
        Handheld.StartActivityIndicator();
        yield return new WaitForSeconds(recordTime / 10);
#if UNITY_IPHONE
        Handheld.StopActivityIndicator();
#elif UNITY_ANDROID
        Handheld.StopActivityIndicator();
#endif
        sharePanel.SetActive(true);
    }

    IEnumerator RecordingProcess()
    {
        while(isRecording)
        {
            yield return new WaitForSeconds(1f);
            //1초에 한번씩 시간갱신
            recordTime++;
            textrecordtime.text = string.Format("{0:d2}:{1:d2}:{2:d2}", recordTime / 3600, (recordTime % 3600) / 60, recordTime % 60);
        }
    }
   

    string FindMP4File()
    {
        Debug.Log("FIND All files");
        string everyplayDir = "";
#if UNITY_IPHONE
		var root = new DirectoryInfo(Application.persistentDataPath).Parent.FullName;
		everyplayDir = root + "/tmp/Everyplay/session";
#elif UNITY_ANDROID
        everyplayDir = Application.temporaryCachePath;
#endif
        string[] files = Directory.GetFiles(everyplayDir, "*.mp4", SearchOption.AllDirectories);
        foreach (string s in files)
        {
            string stemp = s;
            Debug.Log("path = " + stemp);
            return stemp;
        }
        return "";
    }

    public void OnCloseClick()
    {
        if (!isRecording)
        {
            mediaPanel.SetActive(false);
            toolBar.SetActive(true);
        }
    }
		
}