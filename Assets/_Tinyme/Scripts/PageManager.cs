﻿/**
 * Project : Tinyme AR Book
 * Authours: Karlotta Varieur, Thanura Siribaddana
 * Date : 24 Nov 2016
 * */
using UnityEngine;
using System.Collections;

public class PageManager : MonoBehaviour {
	public GroupManager[] groups;

	int groupCount = 0;
	int currentIndex = 0;

	// Use this for initialization
	void Awake () {
		
		groupCount = groups.Length;

		init ();
		Debug.Log ("Page onStart");

	}

	// Update is called once per frame
	void Update () {

	}

	void init () {
		for (int i = 0; i < groupCount; i++) {
			groups [i].setPage (this);
		}

		currentIndex = 0;
	}

	void OnEnable () {
		
		Debug.Log ("Page opened.");

		currentIndex = 0;
	
		showGroup (currentIndex);

		if (ToolBarManager.Instance != null)
			ToolBarManager.Instance.OnPageOpened (this);
	}

	void OnDisable () {
		//Debug.Log ("Page closed.");
		for (int i = 0; i < groupCount; i++) {
			hideGroup (i);
		}

		currentIndex = 0;
		if (ToolBarManager.Instance != null)
			ToolBarManager.Instance.OnPageClosed ();
	}

	void showGroup (int groupId) {
		
		if (groupId >= 0 && groupId < groupCount) {
			//Debug.Log (string.Format("Group{0} opened.", groupId));

			//Moving group in to the marker
			groups [groupId].transform.position = new Vector3(0, 0, 0);

			groups [groupId].showObjects ();

			groups [groupId].transform.position = new Vector3(0, 0, 0);
		
		}
	}

	void hideGroup (int groupId) {
		if (groupId >= 0 && groupId < groupCount) {
			//Debug.Log (string.Format("Group{0} opened.",groupId));

			groups [groupId].hideObjects ();
		}
	}

	public void OnGroupAppeared () {
		
	}

	public bool showNextGroup () {
		hideGroup (currentIndex++);
		showGroup (currentIndex);
		 
		return currentIndex < groupCount - 1;
	}

	public bool showPrevGroup () {
		hideGroup (currentIndex--);
		showGroup (currentIndex);

		return currentIndex > 0;
	}
}
