﻿/**
 * Project : Tinyme AR Book
 * Authours: Thanura Siribaddana
 * Date : 31 March 2017
 * Class: Manage Intro flow screen Transitions
 * */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Transition : MonoBehaviour {


	public enum OutTransitionType { Fade, FadeInstant };
	public enum InTransitionType  { Fade, FadeInstant };

	public OutTransitionType outTransitionType;
	public InTransitionType inTransitionType;

	public string parentTag;
	public Vector3 spawnPosition = Vector3.zero;
	public float fadeSpeed = 2;

	bool transitionInitialized 	= false;
	bool startTransition 		= false;
	float inColorAlpha	= 0;
	float outColorAlpha	= 0;

	Image[] transitionImgs, imgs;

	RectTransform transitionStep;
	RectTransform thisStep;

	// Use this for initialization
	void Start () {
	
		thisStep = GetComponent<RectTransform> ();
		imgs = GetComponentsInChildren<Image> ();


	}

	public GameObject InitialiseTransitionStep ( GameObject transition )
	{
		GameObject obj = Instantiate (transition as GameObject);

		transitionStep = obj.GetComponent<RectTransform> ();
		transitionStep.transform.SetParent(GameObject.FindWithTag(parentTag).transform);
		transitionStep.transform.localScale = Vector3.one;

		transitionImgs = transitionStep.GetComponentsInChildren<Image>();

		foreach (Image i in transitionImgs)
			i.color = new Vector4 (i.color.r, i.color.g, i.color.b, 0);

		transitionInitialized = true;

		return transitionStep.gameObject;


	}	


	public void StartTransition()
	{
		startTransition = true;

	}

	// Update is called once per frame

	void Update () {

		if (startTransition) {

			switch (outTransitionType) {
				case OutTransitionType.Fade:
					FadeStepOut ();
					break;
				case OutTransitionType.FadeInstant:
					inColorAlpha = 0;
				//Destroy (gameObject);
					break;
			}

			switch (inTransitionType) {
				case InTransitionType.Fade:
					FadeStepOut ();
					break;
			case InTransitionType.FadeInstant:
					inColorAlpha = 1;
					break;
			}

			UpdateTransitionStepColors ();
			UpdateCurrentStepColors ();

		}
	
	}


	void FadeStepOut()
	{
		outColorAlpha = Mathf.Lerp (outColorAlpha, 0, fadeSpeed * Time.deltaTime);
	}

	void FadeStepIn()
	{

		inColorAlpha = Mathf.Lerp (inColorAlpha, 1, fadeSpeed * Time.deltaTime);

		if (inColorAlpha > 0.99f)
			inColorAlpha = 1;

		if (inColorAlpha == 1) {

			Destroy (gameObject);

		}
			

	}

	void UpdateTransitionStepColors()
	{

		if (transitionImgs != null) {

			foreach (Image i in transitionImgs)
				i.color = new Vector4 (i.color.r, i.color.g, i.color.b, inColorAlpha);

		}
			
	}


	void UpdateCurrentStepColors()
	{

		if (imgs != null) {

			foreach (Image i in imgs)
				i.color = new Vector4 (i.color.r, i.color.g, i.color.b, outColorAlpha);

		}

	}




}
