﻿/**
 * Project : Tinyme AR Book
 * Authours: Thanura Siribaddana
 * Date : 31 March 2017
 * Class: Manage Intro flow Listners
 * */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DynamicListner : MonoBehaviour {


	public string objectListnerTag = "Enter GameObject's tag";
	public bool thisIsListner = false;
	public bool parameter = false;
	public string sendMessage = "Enter Methos Name";
	public string messageParameter;

	GameObject objectListning;

	Button btn;


	// Use this for initialization
	void Start () {
	
		btn = GetComponent<Button> ();
		GetObjectListening ();
	}

	void GetObjectListening() 
	{

		if (thisIsListner)
			objectListning = gameObject;
		else
			objectListning = GameObject.FindGameObjectWithTag (objectListnerTag);

		if (objectListning)
			SetListner ();

	}

	void SetListner()
	{
		if (btn) {

			if (!parameter)
				btn.onClick.AddListener (() => objectListning.SendMessage (sendMessage));
			else
				btn.onClick.AddListener (() => objectListning.SendMessage (sendMessage, messageParameter));

		} else {

			Debug.LogError ("Dynamic Listner Belongs only on buttons");

		}
	}
	

}
