﻿/**
 * Project : Tinyme AR Book
 * Authours: Thanura Siribaddana
 * Date : 31 March 2017
 * Class: Manage Intro flow screens
 * */
using UnityEngine;
using System.Collections;

public class ScreenController : MonoBehaviour {

	//This is the Gameobject array to hold steps 
	public GameObject[] steps;

	//This is the string array for codes to set each step
	public string[] stepNames;

	GameObject currentStep, toolBar, introFlow, thisPage;


	// Use this for initialization
	void Start () {

		SetCurrentStep (steps[0]);

		toolBar = GameObject.FindGameObjectWithTag ("ToolBar");
		introFlow  = GameObject.FindGameObjectWithTag ("IntroFlow");

		introFlow.SetActive (true);
		toolBar.SetActive (false);

//		if (PlayerPrefs.GetInt ("intro") != 1) {
//			
//			introFlow.SetActive (true);
//			toolBar.SetActive (false);
//
//			PlayerPrefs.SetInt ("intro", 1);
//
//		} else {
//
//			introFlow.SetActive (false);
//			toolBar.SetActive (true);
//			
//		}



	}

	void SetCurrentStep( GameObject step ) 
	{

		GameObject objStep = Instantiate (step as GameObject);

		objStep.transform.SetParent(transform);

		RectTransform rt = objStep.GetComponent<RectTransform>();
		Transition objTrans = objStep.GetComponent<Transition>();
			
		rt.offsetMax = new Vector2 (objTrans.spawnPosition.x, objTrans.spawnPosition.y);
		rt.offsetMin = new Vector2 (objTrans.spawnPosition.x, objTrans.spawnPosition.y);

		RectTransform parent_rt = this.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2(parent_rt.rect.width, parent_rt.rect.height);

		objStep.transform.localScale = Vector3.one;

		currentStep = objStep;
			
	}


	public void SetNextStep (string STEP_CODE ) 
	{

		for (int i = 0; i < stepNames.Length; i++) {

			if (STEP_CODE == stepNames [i]) {

				ShowStep(i);

			}
				
		}

		//Destring current game object, else it'll grow 
		Destroy (thisPage.gameObject,2);
	}
		

	void ShowStep(int index) 
	{

		 
		Transition objTrans = currentStep.GetComponent<Transition> ();
		objTrans.StartTransition();

		thisPage = currentStep;
		currentStep = objTrans.InitialiseTransitionStep (steps [index]);

		RectTransform rt = currentStep.GetComponent<RectTransform> ();
		objTrans = currentStep.GetComponent<Transition> ();

		rt.offsetMax = new Vector2 (objTrans.spawnPosition.x, objTrans.spawnPosition.y);
		rt.offsetMin = new Vector2 (objTrans.spawnPosition.x, objTrans.spawnPosition.y);
	
		RectTransform parent_rt = this.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2(parent_rt.rect.width, parent_rt.rect.height);

		//Debug.Log (thisPage.gameObject.name);
	}

	public void BuyTheBook () {

		Application.OpenURL("https://www.tinyme.com/the-amazing-alphabet-book");

	}


	public void WatchVideo () {
		
		Application.OpenURL("https://www.youtube.com/watch?v=dabfQz0DIOg");

	}


	public void DownloadTestPage () {
	
		Application.OpenURL("https://www.tinyme.com/the-amazing-alphabet-demo");

	}

	public void ImReady() {

		introFlow.SetActive (false);
		toolBar.SetActive (true);

	}

	public void SkipIntro() {

		toolBar.SetActive (true);

	}
}
