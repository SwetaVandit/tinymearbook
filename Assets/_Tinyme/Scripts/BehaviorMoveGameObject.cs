﻿/**
 * Project : Tinyme AR Book
 * Authours:  Thanura Siribaddana
 * Date : 24 Nov 2016
 * */
using UnityEngine;
using System.Collections;

public class BehaviorMoveGameObject : StateMachineBehaviour {

	 
	public float Position_X;
	public float Position_Y;
	public float Position_Z;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{

		animator.gameObject.transform.position = new Vector3 (Position_X, Position_Z, Position_Y);

	}
		
}
