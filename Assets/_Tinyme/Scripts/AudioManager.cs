﻿/**
 * Project : Tinyme AR Book
 * Authours: Karlotta Varieur, Thanura Siribaddana
 * Date : 24 Nov 2016
 * */
using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
	public static AudioManager Instance;

	AudioSource[] source;

	AudioSource ReadtoMe;
	AudioSource BgMusic;

	float clipTime = 0; //Read to me clip
	float playedTime = 0;
	bool isPlaying = false;

	float bgVolume = 0.3f;
	float bgDuckedVolume = 0.2f;

	// Use this for initialization
	void Awake () {
		
		Instance = this;
		source = GetComponents<AudioSource> ();

		ReadtoMe = source [0];
		BgMusic = source [1];


	}

	// Update is called once per frame
	void Update () {
		
		if (isPlaying) {
			
			playedTime += Time.deltaTime;

			if (playedTime > clipTime) {
				
				stopAudio ();
				playedTime = 0;

				ToolBarManager.Instance.OnAudioEnd ();
			}
		}
	}

	public void setAudio (AudioClip clip) {
		
		if (ReadtoMe != null && clip != null) {
			
			ReadtoMe.clip = clip;
			clipTime = clip.length;
			playedTime = 0;

		} else {

			ReadtoMe.clip = null;
		}

	}
		
	public void playAudio () {
		
		if (ReadtoMe != null ) {
			
			ReadtoMe.Play ();
			isPlaying = true;
			setBgVolume (bgDuckedVolume);
		}

	}

	public void pauseAudio () {
		
		if (ReadtoMe != null) {
			
			ReadtoMe.Pause ();
			isPlaying = false;
			setBgVolume (bgVolume);
		}
	}

	public void stopAudio () {
		
		if (ReadtoMe != null) {
			
			ReadtoMe.Stop ();
			isPlaying = false;
			setBgVolume (bgVolume);
		}

	}

	public void setVolume (float volume) {
		ReadtoMe.volume = volume;
	}

	// Background Music Audio source functions

	public void setBgAudio (AudioClip clip) {
		
		if (BgMusic != null && clip != null) {
			
			BgMusic.clip = clip;

		} else {
			
			BgMusic.clip = null;
		}

	}

	public void playBgAudio () {
		
		if (BgMusic != null) {
			
			BgMusic.Play ();

		}

	}

	public void stopBgAudio () {
		
		if (BgMusic != null) {
			
			BgMusic.Stop ();

		}

	}

	public void setBgVolume (float volume) {
		
		BgMusic.volume = volume;

	}
}
