﻿/**
 * Project : Tinyme AR Book
 * Authours: Thanura Siribaddana
 * Date : 24 Nov 2016
 * */
using UnityEngine;
using System.Collections;

public class ObjectManager : MonoBehaviour {

	public float AnimationDelay;
	Animator anim;

	public IEnumerator ShowObject (GameObject cur_object) {

		yield return new WaitForSeconds (AnimationDelay);
		cur_object.SetActive (true);

	}
}
